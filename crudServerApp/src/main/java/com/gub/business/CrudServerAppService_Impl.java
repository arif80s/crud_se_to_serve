/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gub.business;

import com.gub.model.User;
import com.gub.persistence.CrudServerAppCrud_I;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author abra
 */
@Component
@Qualifier("RealService")
public class CrudServerAppService_Impl implements CrudServerAppService_I
{
    @Autowired
    private CrudServerAppCrud_I crudServerAppCrud_I;

    public CrudServerAppCrud_I getCrudServerAppCrud_I() 
    {
        return crudServerAppCrud_I;
    }

    public void setCrudServerAppCrud_I(CrudServerAppCrud_I crudServerAppCrud_I) 
    {
        this.crudServerAppCrud_I = crudServerAppCrud_I;
    }
    
    
    @Override
    public void createUser(User user) 
    {
        crudServerAppCrud_I.save(user);
    }

    @Override
    public User readUser(int id) 
    {
        return crudServerAppCrud_I.findOne(id);
    }

    @Override
    public void deleteUser(int id) 
    {
        crudServerAppCrud_I.delete(id);
    }

    @Override
    public int update(String email, int id) 
    {
       return crudServerAppCrud_I.setEmailForId(email, id);
    }

 
    
}

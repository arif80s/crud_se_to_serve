/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gub.business;

import com.gub.model.User;
import org.springframework.stereotype.Service;

/**
 *
 * @author abra
 */
@Service
public interface CrudServerAppService_I 
{
    public void createUser(User user);
    public User readUser(int id);
    public void deleteUser(int id);
    public int update(String email, int id);
}

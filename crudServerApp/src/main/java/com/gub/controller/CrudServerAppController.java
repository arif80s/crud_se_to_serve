/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gub.controller;

import com.gub.business.CrudServerAppService_I;
import com.gub.model.IdEmailUser;
import com.gub.model.IdUser;
import com.gub.model.User;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abra handle http request
 */
@RestController
public class CrudServerAppController 
{
    @Autowired
    private CrudServerAppService_I crudServerAppService_I;

    public CrudServerAppService_I getCrudServerAppService_I() 
    {
        return crudServerAppService_I;
    }

    public void setCrudServerAppService_I(CrudServerAppService_I crudServerAppService_I) 
    {
        this.crudServerAppService_I = crudServerAppService_I;
    }
    
    
    @PostMapping("/create")
    public String saveData(@RequestBody User user, HttpServletRequest httpServletRequest) 
    {
        crudServerAppService_I.createUser(user);
        return "Done..!";
    }
    
    @PostMapping("/read")
//    @GetMapping("/read")
    public User readData(@RequestBody IdUser idUser, HttpServletRequest httpServletRequest) 
    {
        return crudServerAppService_I.readUser(idUser.getId());
    }
    
    @PutMapping("/update")
    public int updateData(@RequestBody IdEmailUser idEmailUser, HttpServletRequest httpServletRequest) 
    {
        return crudServerAppService_I.update(idEmailUser.getEmail(), idEmailUser.getId());
    }
     
    @DeleteMapping("/delete")
    public void removeData(@RequestBody IdUser idUser, HttpServletRequest httpServletRequest) 
    {
        crudServerAppService_I.deleteUser(idUser.getId());
    }
    
}

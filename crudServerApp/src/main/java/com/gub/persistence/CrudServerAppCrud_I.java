/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gub.persistence;

import com.gub.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abra
 */
public interface CrudServerAppCrud_I extends CrudRepository<User, Integer>
{
    
    @Override
    public User save(User user);
    
    @Override
    public User findOne(Integer id);
    
    @Override
    public void delete(Integer id);
    
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE User c SET c.email = :email WHERE c.id = :id")
    int setEmailForId(@Param("email")String email, @Param("id")int id);


}

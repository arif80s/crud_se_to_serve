package com.gub.crudServerApp;

import com.gub.controller.CrudServerAppController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.gub.persistence")
@ComponentScan(basePackageClasses = CrudServerAppController.class)
@ImportResource("classpath:spconfig.xml")
public class CrudServerAppApplication 
{

    public static void main(String[] args) 
    {
            SpringApplication.run(CrudServerAppApplication.class, args);
            System.out.println("helooo crudserverapp!!!!");
    }
    
}

package com.objProvide;

//@Arif

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.IdEmailUser;
import com.model.IdUser;
import com.model.User;

//convert Pojo to json object
public class JsonObjectProvider 
{
    
    String jsonInString;

//  return json formatting object
    public String userJsonStringCreate(User user) 
    {
        ObjectMapper mapper = new ObjectMapper();

        try 
        {
//          make json formatting string
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);

        } 
        catch (JsonGenerationException e) 
        {
        } 
        catch (JsonMappingException e) 
        {
        } 
        catch (IOException e) 
        {
        }
        
        return jsonInString;
    
    }
    public String IdUserJsonStringCreate(IdUser idUser) 
    {
        ObjectMapper mapper = new ObjectMapper();

        try 
        {
//          make json formatting string
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(idUser);

        } 
        catch (JsonGenerationException e) 
        {
        } 
        catch (JsonMappingException e) 
        {
        } 
        catch (IOException e) 
        {
        }
        
        return jsonInString;
    
    }
    public String IdEmailUserJsonStringCreate(IdEmailUser idEmailUser) 
    {
        ObjectMapper mapper = new ObjectMapper();

        try 
        {
//          make json formatting string
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(idEmailUser);

        } 
        catch (JsonGenerationException e) 
        {
        } 
        catch (JsonMappingException e) 
        {
        } 
        catch (IOException e) 
        {
        }
        
        return jsonInString;
    
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;

/**
 *
 * @author abra
 */
//class use to make request with id and email field
public class IdEmailUser implements Serializable
{
    private int id;
    private String email;

    public IdEmailUser() 
    {
        
    }

    public IdEmailUser(int id, String email) 
    {
        this.id = id;
        this.email = email;
    }

    public int getId() 
    {
        return id;
    }

    public void setId(int id) 
    {
        this.id = id;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }
    
    
}

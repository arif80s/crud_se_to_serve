/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 *
 * @author abra
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable
{
    private int version;
//    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private int phone;
    private String address;
    
    public User()
    {
        
    }

    public User(int version, String firstName, String lastName, String email, int phone, String address)
    {
        this.version = version;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public String getFirstName() 
    {
        return firstName;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public String getLastName() 
    {
        return lastName;
    }

    public void setLastName(String lastName) 
    {
        this.lastName = lastName;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public int getPhone() 
    {
        return phone;
    }

    public void setPhone(int phone) 
    {
        this.phone = phone;
    }
    public int getVersion() 
    {
        return phone;
    }

    public void setVersion(int phone) 
    {
        this.phone = phone;
    }

    public String getAddress() 
    {
        return address;
    }

    public void setAddress(String address) 
    {
        this.address = address;
    }

     
 
}

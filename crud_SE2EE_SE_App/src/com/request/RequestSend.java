package com.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author abra
 */

//receive json string and send request to localhost
public class RequestSend 
{
    
//  send request to save a User object in the database
    public int sendPostRequest(String jsonString) throws MalformedURLException, IOException
    {
        
        URL url = new URL("http://localhost:8080/create");
        
//      create connection to the url
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        
        connection.setConnectTimeout(5000);//5 secs
        connection.setReadTimeout(5000);//5 secs
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        
//      make outputstream writer with connection to send the post request        
        try (OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream())) 
        {
            out.write(jsonString);
            out.flush();
        }

        int response = connection.getResponseCode();
        
//      finally close the connection
        connection.disconnect();
        
        return response;

    }
    
//  send request to read a User object from the database    
    public User sendGetRequest(String jsonString) throws MalformedURLException, IOException
    {
        URL url = new URL("http://localhost:8080/read");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        
        conn.setRequestMethod("GET");
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-TYpe", "application/json");
        
//      request send to server with jsonString by calling write function and connect
        conn.getOutputStream().write(jsonString.getBytes());
        conn.connect();
        
        StringBuffer response;
        
//      in object hold response back from server as stream
//      and convert stream to json formatted string
        try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) 
        {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
            {
                response.append(inputLine);
            }
        }
//      finally map to the user object        
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = response.toString();
        User user1 = mapper.readValue(jsonInString, User.class);
        
//      close the connection
        conn.disconnect();
        
        return user1;
 

    }
    
//  send request to update a User object in the database    
    public int sendPutRequest(String jsonString) throws MalformedURLException, IOException
    {
        URL url = new URL("http://localhost:8080/update");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        
        conn.setRequestMethod("PUT");
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-TYpe", "application/json");
//      start : send PUT request
        conn.getOutputStream().write(jsonString.getBytes());
        conn.connect();
//      end : send PUT request
        
        int responsecode = conn.getResponseCode();
        conn.disconnect();
        
        return responsecode;
 

    }
    
//  send request to delete a User object in the database    
    public int sendDeleteRequest(String jsonString) throws MalformedURLException, IOException
    {
        URL url = new URL("http://localhost:8080/delete");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        
        conn.setRequestMethod("DELETE");
        conn.setDoOutput(true);
        conn.addRequestProperty("Content-TYpe", "application/json");
        
//      convert json formatted string to bytes then after writing by calling connect method send the request
        conn.getOutputStream().write(jsonString.getBytes());
        
        conn.connect();
        int responsecode = conn.getResponseCode();
        conn.disconnect();
        
        return responsecode;
 

    }
}

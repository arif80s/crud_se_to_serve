/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package crudseapp;

import com.model.IdEmailUser;
import com.model.IdUser;
import com.model.User;
import com.objProvide.JsonObjectProvider;
import com.request.RequestSend;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author abra
 */
public class CrudSeApp {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException 
    {
        // TODO code application logic here
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to operation CRUD!!\n");
        System.out.println("give an input 1 through 4 for the desired C(1) R(2) U(3) D(4)\n");

//      String s = scan.next();
        int operationNo = scan.nextInt();
        if(operationNo == 1)
        {
            System.out.println("version : ");
            int version = scan.nextInt();
            System.out.println("Firstname : ");
            String fname = scan.next();
            System.out.println("Lasname : ");
            String lname = scan.next();
            System.out.println("Email : ");
            String email = scan.next();
            System.out.println("PhoneNumber : ");
            int phone = scan.nextInt();
            System.out.println("Address : ");
            String address = scan.next();
            
            User user = new User(version, fname, lname, email, phone, address);
            JsonObjectProvider jsonObjectProvider = new JsonObjectProvider();
            //send user model object and receive json String
            String jsonData = (String)jsonObjectProvider.userJsonStringCreate(user);
            RequestSend postReqSend = new RequestSend();
            //call method of RequestSend class that forward request to server and give response
            if(postReqSend.sendPostRequest(jsonData) == 200)
            {
                System.out.println("User object saved successfully!!!");
            }
        }//if(operationNo == 1)
        
        if(operationNo == 2)
        {
            System.out.println("Id : ");
            int id = scan.nextInt();
            
            IdUser idUser = new IdUser(id);
            JsonObjectProvider jsonObjectProvider = new JsonObjectProvider();
            
            //send user model object and receive json String
            String jsonData = jsonObjectProvider.IdUserJsonStringCreate(idUser);
            RequestSend gettReqSend = new RequestSend();
            User user = gettReqSend.sendGetRequest(jsonData);
            System.out.println("\nFirstName : "+user.getFirstName()+"\n");
            System.out.println("LastName : "+user.getLastName()+"\n");
            System.out.println("Email : "+user.getEmail()+"\n");
            System.out.println("Phone : "+user.getPhone()+"\n");
            System.out.println("Address : "+user.getAddress()+"\n");
        }//if(operationNo == 2)
        
        if(operationNo == 3)
        {
            System.out.println("Id : ");
            int id = scan.nextInt();
            System.out.println("Give New Email Address : ");
            String newEmail = scan.next();
            
            IdEmailUser idEmailUser = new IdEmailUser(id, newEmail);
            JsonObjectProvider jsonObjectProvider = new JsonObjectProvider();
            
            //send user model object and receive json String
            String jsonData = jsonObjectProvider.IdEmailUserJsonStringCreate(idEmailUser);
            RequestSend putReqSend = new RequestSend();
            
            //call method of RequestSend class that forward request to server and give response
            int response = putReqSend.sendPutRequest(jsonData);
            if(response == 200)
            {
                System.out.println("data updated successfully!!!");
            }    
        }//if(operationNo == 3)
        
        if(operationNo == 4)
        {
            System.out.println("Id : ");
            int id = scan.nextInt();
 
 
            IdUser idUser = new IdUser(id);
            JsonObjectProvider jsonObjectProvider = new JsonObjectProvider();
            
            //send user model object and receive json String
            String jsonData = jsonObjectProvider.IdUserJsonStringCreate(idUser);
            RequestSend deleteReqSend = new RequestSend();
            
            //call method of RequestSend class that forward request to server and give response
            int response = deleteReqSend.sendDeleteRequest(jsonData);
            
            if(response == 200)
            {
                System.out.println("data deleted successfully!!!");
            }    
        }//if(operationNo == 4)
        
        scan.close();
    }//main
}//class
